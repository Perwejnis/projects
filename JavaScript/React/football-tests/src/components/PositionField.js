import * as React from 'react';
import * as Form from './Form';
import PropTypes from 'prop-types';

export default function PositionField(props) {
    return <Form.Field type="select" {...props}>
        <option value="">Wybierz pozycję</option>
        {renderOptions()}
    </Form.Field>;
}

function renderOptions() {
    const options = ['Bramkarz', 'Obrońca', 'Pomocnik', 'Napastnik'];
    return options.map((option, index) => {
        return <option key={index} value={option}>{option}</option>;
    });
}

PositionField.propTypes = {
    name: PropTypes.string.isRequired,
    label: PropTypes.string,
    placeholder: PropTypes.string
};

PositionField.defaultProps = {
    placeholder: "Podaj pozycje..."
};