import * as React from 'react';
import PropTypes from 'prop-types';

export default class FootballTestsTable extends React.PureComponent {

    render() {
        const {tests, hasFilters} = this.props;

        return <div className="c-football-tests-table__container">
            {hasFilters && tests.length === 0 && this.renderEmptyTestsState()}
            {!hasFilters && tests.length === 0 && this.renderEmptyTableNote()}
            {tests.length > 0 && this.renderTable()}
        </div>;
    }

    renderEmptyTestsState() {
        const {onResetFilters} = this.props;
        return <div className="p-football-tests__empty-state">
            <h2 className="p-football-tests__no-results-header">Nie znaleziono testów z wybranymi filtrami.</h2>

            <button className='btn btn-primary p-football-tests__reset-filters' onClick={onResetFilters}>Resetuj
                filtry
            </button>
        </div>;
    }

    renderEmptyTableNote() {
        return <h2 className="c-football-tests-table__header">Aktualnie nie ma żadnych dostępnych testów</h2>;
    }

    renderTable() {
        const {tests} = this.props;
        return <table className="table table-hover">
            {this.renderTableHeaders()}
            <tbody>
            {tests.map(this.renderItem, this)}
            </tbody>
        </table>;
    }

    renderTableHeaders() {
        return <thead className="thead-inverse c-football-tests-table__headers">
        <tr>
            <th></th>
            <th>Miasto</th>
            <th>Nazwa klubu</th>
            <th>Klasa rozgrywkowa</th>
            <th>Pozycja</th>
            <th>Wynagrodzenie</th>
            <th>Liczba miejsc</th>
            <th>Kontakt</th>
            <th>Akcje</th>
        </tr>
        </thead>;
    }

    renderItem(element, index) {
        return <tr key={element.id} className="c-football-tests-table__item">
                <td>{index + 1}</td>
                {this.renderTestCells(element)}
                <td>
                    {this.renderEditItemButton(element.id)}
                    {this.renderRemoveItemButton(element.id)}
                </td>
        </tr>;
    }

    renderTestCells(element) {
        const {selectTestAndOpenModal} = this.props;

        return Object.keys(element).map((key, index) => {
            return key !== 'id' && <td key={index} onClick={() => {selectTestAndOpenModal(element)}}>{element[key]}</td>;
        });
    }

    renderEditItemButton(index) {
        const {onEditTest} = this.props;
        return <button className='c-football-tests-table__cta-button' onClick={() => {
            onEditTest(index)
        }}>
            <i className="fa fa-pencil"></i>
        </button>;
    }

    renderRemoveItemButton(id) {
        const {onRemoveTest} = this.props;
        return <button className="c-football-tests-table__cta-button" onClick={() => {
            onRemoveTest(id)
        }}>
            <i className="fa fa-trash"></i>
        </button>;
    }
}

FootballTestsTable.propTypes = {
    tests: PropTypes.arrayOf(
        PropTypes.shape({
            city: PropTypes.string,
            clubName: PropTypes.string,
            position: PropTypes.string,
            league: PropTypes.string,
            salary: PropTypes.number,
            vacancies: PropTypes.number,
            contact: PropTypes.string,
        })
    ),
    selectTestAndOpenModal: PropTypes.func.isRequired,
    onRemoveTest: PropTypes.func.isRequired,
    onEditTest: PropTypes.func.isRequired,
    onResetFilters: PropTypes.func.isRequired,
    hasFilters: PropTypes.bool.isRequired
};

FootballTestsTable.defaultProps = {
    filters: {},
    tests: []
};
