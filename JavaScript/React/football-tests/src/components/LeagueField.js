import * as React from 'react';
import * as Form from './Form';
import PropTypes from 'prop-types';

export default function LeagueField({...props}) {
    return <Form.Field type="select" {...props}>
        <option value="">Wybierz klasę rozgrywkową</option>
        {renderOptions()}
    </Form.Field>;
}

function renderOptions() {
    const options = ['5', '4', '3', '2', '1', 'Ekstraklasa'];
    return options.map((option, index) => {
        return <option key={index} value={option}>{option}</option>;
    });
}

LeagueField.propTypes = {
    name: PropTypes.string.isRequired,
    label: PropTypes.string,
    placeholder: PropTypes.string
};

LeagueField.defaultProps = {
    placeholder: "Podaj klasę rozgrywkową..."
};