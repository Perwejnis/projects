import * as React from 'react';
import Autocomplete from "./Autocomplete";

const clubs = [
    {id: 'arka', label: 'Lechia Gdańsk'},
    {id: 'legia', label: 'Legia Warszawa'},
    {id: 'krakow', label: 'Wisła Kraków'},
    {id: 'lech_poznan', label: 'Lech Poznań'},
    {id: 'warta_poznan', label: 'Warta Poznań'},
    {id: 'wroclaw', label: 'Śląsk Wrocław'},
    {id: 'paslek', label: 'Polonia Pasłęk'},
    {id: 'lodz', label: 'Łódzki Klub Sportowy'},
    {id: 'lublin', label: 'Zagłębie Lublin'},
    {id: 'rzeszow', label: 'Stal Rzeszów'},
    {id: 'gdynia', label: 'Arka Gdynia'},
    {id: 'elblag', label: 'Olimpia Elbląg'},
];

export default function ClubNameAutocomplete(props) {
    return <Autocomplete label="Klub" placeholder="Podaj nazwę klubu..." items={clubs} {...props}/>;
}
