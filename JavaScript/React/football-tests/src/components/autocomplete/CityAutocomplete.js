import * as React from 'react';
import Autocomplete from "./Autocomplete";

const cities = [
    {id: 'gdansk', label: 'Gdańsk'},
    {id: 'warszawa', label: 'Warszawa'},
    {id: 'krakow', label: 'Kraków'},
    {id: 'poznan', label: 'Poznań'},
    {id: 'wroclaw', label: 'Wrocław'},
    {id: 'olsztyn', label: 'Szczecin'},
    {id: 'bialystok', label: 'Białystok'},
    {id: 'gorzow_wielkopolski', label: 'Gorzów Wielkopolski'},
    {id: 'katowice', label: 'Katowice'},
    {id: 'kielce', label: 'Kielce'},
    {id: 'lodz', label: 'Łódź'},
    {id: 'zielona_gora', label: 'Zielona Góra'},
    {id: 'lublin', label: 'Lublin'},
    {id: 'rzeszow', label: 'Rzeszów'},
    {id: 'opole', label: 'Opole'},
    {id: 'gdynia', label: 'Gdynia'},
    {id: 'elblag', label: 'Elbląg'},
];

export default function CityAutocomplete(props) {
    return <Autocomplete label="Miasto" placeholder="Podaj miasto..." items={cities} {...props}/>;
}