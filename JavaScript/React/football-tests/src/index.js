import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import 'bootstrap/dist/css/bootstrap.css';
import 'font-awesome/css/font-awesome.css';
import 'react-notifications/lib/notifications.css';
import './styles.css';

ReactDOM.render(<App/>, document.getElementById('root'));
