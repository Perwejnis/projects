import * as React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

export function Button({className, isPrimary, isDanger, isSuccess, isWarning,isSecondary, children, ...otherProps}) {

    const classes = classNames('btn c-button',{
        'btn-primary': isPrimary,
        'btn-danger': isDanger,
        'btn-success': isSuccess,
        'btn-warning': isWarning,
        'btn-secondary': isSecondary,
    }, className);

    return <button className={classes} {...otherProps}>{children}</button>;
}

Button.propTypes = {
    isPrimary: PropTypes.bool,
    isDanger: PropTypes.bool,
    isSuccess: PropTypes.bool,
    isWarning: PropTypes.bool,
    isSecondary: PropTypes.bool,
    children: PropTypes.node,
};