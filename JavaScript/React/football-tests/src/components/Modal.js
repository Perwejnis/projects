import * as React from 'react';
import ReactModal from "react-modal";
import PropTypes from 'prop-types';
import classNames from 'classnames';
import {Button} from "./Button";

export class Modal extends React.PureComponent {

    render() {
        const {isOpen, onClose, title, children, isBig, className} = this.props;
        const classes = classNames('c-modal', {'c-modal--big': isBig}, className);
        return <ReactModal
            isOpen={isOpen}
            contentLabel="Modal"
            ariaHideApp={false}
            className={classes}
        >
            <div className="c-modal__title-with-close-button">
                <h3 className="c-modal__title">{title}</h3>
                <CloseButton onClose={onClose}/>
            </div>
            {children}
        </ReactModal>;
    }

}

Modal.propTypes = {
    isOpen: PropTypes.bool.isRequired,
    onClose: PropTypes.func.isRequired,
    title: PropTypes.string.isRequired,
    isBig: PropTypes.bool
};


export function ModalContent({className, children}) {
    const classes = classNames('c-modal__content', className);
    return <div className={classes}>
        {children}
    </div>;
}

export function ModalFooter({className, children}) {
    const classes = classNames('c-modal__footer', className);
    return <div className={classes}>
        {children}
    </div>;
}

export function CloseButton({onClose}) {
    return <button className="c-modal__close-button" onClick={onClose}>
        <i className="fa fa-times fa-3" aria-hidden="true"></i>
    </button>;
}

CloseButton.propTypes = {
    onClose: PropTypes.func.isRequired
};

export function ModalButtons({submitTitle, cancelTitle, onSubmit, onCancel, isDanger, isSuccess, isDisabled}) {
    return <div className="c-modal__buttons">
        <Button className='c-modal__buttons__close' onClick={onCancel} isSecondary={true}>{cancelTitle}</Button>
        <Button isDanger={isDanger} isSuccess={isSuccess} disabled={isDisabled} onClick={onSubmit}>{submitTitle}</Button>
    </div>;
}

ModalButtons.propTypes = {
    submitTitle: PropTypes.string,
    cancelTitle: PropTypes.string,
    onSubmit: PropTypes.func.isRequired,
    isDisabled: PropTypes.bool,
    onCancel: PropTypes.func.isRequired,
    isDanger: PropTypes.bool,
    isSuccess: PropTypes.bool,
};

ModalButtons.defaultProps = {
    submitTitle: 'Zatwierdź',
    cancelTitle: 'Anuluj',
};

export function ConfirmDialogue({isOpen, isDanger, isSuccess, question, description, onClose, className, ...otherProps}) {
    const modalClasses = classNames('c-modal-confirm', className);
    return <Modal isOpen={isOpen}
                  className={modalClasses}
                  onClose={onClose}
                  title={question}>
        {!!description && <ModalContent>
            <h3 className="c-modal-confirm__description">{description}</h3>
        </ModalContent>}
        <ModalFooter>
            <ModalButtons onCancel={onClose} isDanger={isDanger} isSuccess={isSuccess} {...otherProps}/>
        </ModalFooter>
    </Modal>;
}

ConfirmDialogue.propTypes = {
    isOpen: PropTypes.bool.isRequired,
    question: PropTypes.string.isRequired,
    description: PropTypes.string,
    submitTitle: PropTypes.string,
    cancelTitle: PropTypes.string,
    onSubmit: PropTypes.func.isRequired,
    onClose: PropTypes.func.isRequired,
};
