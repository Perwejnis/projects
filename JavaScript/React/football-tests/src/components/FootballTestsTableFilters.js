import * as React from 'react';
import PropTypes from 'prop-types';
import yup from 'yup';
import * as Form from './Form';
import LeagueField from "./LeagueField";
import PositionField from "./PositionField";
import CityAutocomplete from "./autocomplete/CityAutocomplete";
import ClubNameAutocomplete from "./autocomplete/ClubNameAutocomplete";

const filtersSchema = yup.object({
    city: yup.string(),
    position: yup.string(),
    clubName: yup.string(),
    league: yup.string(),
});

export default class TableFilters extends React.PureComponent {

    render() {
        const {filters, onChange} = this.props;
        return <div className="c-football-tests-table-filters__container">
            <Form.Form
                className="c-football-tests-table-filters__form"
                schema={filtersSchema}
                onChange={onChange}
                value={filters}
            >
                <Form.Input name="city" type={CityField} label="Miasto"/>
                <Form.Input name="clubName" type={ClubNameField} label="Nazwa klubu"/>
                <PositionField name="position" label="Pozycja"/>
                <LeagueField name="league" label="Klasa rozgrywkowa"/>
            </Form.Form>
        </div>;
    }
}

function CityField(props) {
    return <CityAutocomplete {...props}/>
}

function ClubNameField(props) {
    return <ClubNameAutocomplete {...props}/>;
}

TableFilters.propTypes = {
    onChange: PropTypes.func.isRequired
};
