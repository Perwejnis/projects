import * as React from 'react';
import PropTypes from 'prop-types';
import yup from 'yup';
import * as Modal from "./Modal";
import * as Form from './Form';
import PositionField from "./PositionField";
import LeagueField from "./LeagueField";
import CityAutocomplete from "./autocomplete/CityAutocomplete";
import ClubNameAutocomplete from "./autocomplete/ClubNameAutocomplete";

const schema = yup.object({
    city: yup.string().required("Miasto jest wymagane"),
    clubName: yup.string().required(1, 'Nazwa klubu jest wymagana'),
    position: yup.string().required('Pozycja jest wymagana'),
    league: yup.string().required('Klasa rozgrywkowa jest wymagana'),
    salary: yup.number().required('Wynagrodzenie jest wymagane').default(0),
    vacancies: yup.number().required('Liczba miejsc jest wymagana').default(1),
    contact: yup.string().required('Kontakt jest wymagany')
});

const defaultNewTest = {
    city: '',
    clubName: '',
    position: '',
    league: '',
    salary: 0,
    vacancies: 1,
    contact: '',
};

export default class AddTestModal extends React.PureComponent {

    constructor(props) {
        super(props);

        this.state = {
            newTest: defaultNewTest,
            isValid: false
        };

        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    render() {
        const {isValid} = this.state;
        const {onClose, isOpen} = this.props;

        return <Modal.Modal
            isOpen={isOpen}
            onClose={onClose}
            isBig={true}
            title="Dodaj nowy test"
        >
            <Modal.ModalContent>
                {this.renderForm()}
            </Modal.ModalContent>
            <Modal.ModalFooter>
                <Modal.ModalButtons
                    submitTitle="Dodaj test"
                    isSuccess={true}
                    isDisabled={!isValid}
                    onCancel={onClose}
                    onSubmit={this.onSubmit}/>
            </Modal.ModalFooter>
        </Modal.Modal>;
    }

    renderForm() {
        const {newTest} = this.state;
        return <Form.Form
            schema={schema}
            ref={form => {this.form = form}}
            onSubmit={this.onSubmit}
            onChange={this.onChange}
            value={newTest}
        >
            <Form.Input name="city" type={CityField} label="Miasto"/>
            <Form.Input name="clubName" type={ClubNameField} label="Nazwa klubu"/>
            <PositionField name="position" label="Pozycja"/>
            <LeagueField name="league" label="Klasa rozgrywkowa"/>
            <Form.Field type='number' name="salary" label="Wynagrodzenie" placeholder="Podaj wynagrodzenie"/>
            <Form.Field name="vacancies" type='number' label="Liczba wakatów" placeholder="Podaj liczbę miejsc..."/>
            <Form.Field name="contact" label="Kontakt" placeholder="Podaj numer kontaktowy..."/>
        </Form.Form>;
    }

    onChange(newTest) {
        schema.isValid(newTest).then(isValid => {
            this.setState({
                isValid,
                newTest
            });
        });
    }

    onSubmit() {
        this.setState({
            newTest: defaultNewTest
        });

        this.props.onSubmit(this.state.newTest);
    }
}

function CityField(props) {
    return <CityAutocomplete {...props}/>
}

function ClubNameField(props) {
    return <ClubNameAutocomplete {...props}/>;
}

AddTestModal.propTypes = {
    isOpen: PropTypes.bool.isRequired,
    onClose: PropTypes.func.isRequired,
    onSubmit: PropTypes.func.isRequired,
};
