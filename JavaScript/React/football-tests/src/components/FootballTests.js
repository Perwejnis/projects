import * as React from 'react';
import * as firebase from '../firebase';
import FootballTestsTable from "./../components/FootballTestsTable";
import FootballTestsTableFilters from "./../components/FootballTestsTableFilters";
import {TestDetailsModal} from "./../components/TestDetailsModal";
import AddTestModal from "./../components/AddTestModal";
import EditTestModal from "./../components/EditTestModal";
import {ConfirmDialogue} from "./../components/Modal";
import {Button} from "./../components/Button";
import Notifications from "./../components/Notifications";
import LoadingScreen from "./LoadingScreen";
import {availableTests} from "../Mocks";

const fieldsFilteredByPhrase = ['city', 'clubName'];
const fieldsFilteredByCompare = ['position', 'league'];

class App extends React.PureComponent {

    constructor(props) {
        super(props);
        const availableTests = [];

        firebase.getTests().then(tests => {

            Object.keys(tests).forEach(key => {
                availableTests.push(tests[key]);
            });

            this.setState({
                availableTests,
                requestInProgress: false
            });

        });

        this.state = {
            selectedTest: null,
            availableTests,
            filters: {
                city: '',
                clubName: '',
                league: '',
                position: ''
            },
            testDetailModalIsOpen: false,
            addTestModalIsOpen: false,
            editTestModalIsOpen: false,
            confirmDeleteModalIsOpen: false,
            requestInProgress: true
        };

        this.selectTestAndOpenModal = this.selectTestAndOpenModal.bind(this);
        this.closeTestDetailsModal = this.closeTestDetailsModal.bind(this);
        this.resetFilters = this.resetFilters.bind(this);
        this.onRemoveTestClick = this.onRemoveTestClick.bind(this);
        this.onCloseConfirmRemoveTestModal = this.onCloseConfirmRemoveTestModal.bind(this);
        this.onRemoveTest = this.onRemoveTest.bind(this);
        this.toggleAddTestModal = this.toggleAddTestModal.bind(this);
        this.addTest = this.addTest.bind(this);
        this.openEditTestModal = this.openEditTestModal.bind(this);
        this.closeEditTestModal = this.closeEditTestModal.bind(this);
        this.onEditTest = this.onEditTest.bind(this);
        this.onFiltersChange = this.onFiltersChange.bind(this);
        this.refetchTests = this.refetchTests.bind(this);
    }


    render() {
        const {requestInProgress} = this.state;
        return requestInProgress ? <LoadingScreen/> : this.renderContent();
    }

    renderContent() {
        const {filters, availableTests, selectedTest, testDetailModalIsOpen, addTestModalIsOpen, editTestModalIsOpen} = this.state;
        const filteredTests = this.filterTests();
        return <div className="App p-football-tests">
            <h2 className="p-football-tests__header">Lista dostępnych testów piłkarskich</h2>
            <Button onClick={this.toggleAddTestModal} isPrimary={true}>Dodaj nowy test</Button>
            {availableTests.length > 0 && <FootballTestsTableFilters
                filters={filters}
                onChange={this.onFiltersChange}/>}
            <FootballTestsTable
                tests={filteredTests}
                filters={filters}
                hasFilters={this.hasFilters}
                onResetFilters={this.resetFilters}
                onRemoveTest={this.onRemoveTestClick}
                onEditTest={this.openEditTestModal}
                selectTestAndOpenModal={this.selectTestAndOpenModal}
            />
            <TestDetailsModal isOpen={testDetailModalIsOpen}
                              onClose={this.closeTestDetailsModal}
                              selectedTest={selectedTest}/>
            <AddTestModal isOpen={addTestModalIsOpen}
                          onSubmit={this.addTest}
                          onClose={this.toggleAddTestModal}/>
            <EditTestModal isOpen={editTestModalIsOpen}
                           selectedTest={selectedTest}
                           onEdit={this.onEditTest}
                           onClose={this.closeEditTestModal}/>
            {this.renderConfirmRemoveTestModal()}
        </div>;
    }

    renderConfirmRemoveTestModal() {
        const {confirmDeleteModalIsOpen} = this.state;
        return <ConfirmDialogue isOpen={confirmDeleteModalIsOpen}
                                question="Usuwanie testu"
                                description="Czy na pewno chcesz usunąć wybrany test?"
                                submitTitle="Usuń"
                                isDanger={true}
                                onSubmit={this.onRemoveTest}
                                onClose={this.onCloseConfirmRemoveTestModal}/>;
    }

    onFiltersChange(filters) {
        this.setState({
            filters: {
                ...filters
            }
        })
    }

    resetFilters() {
        this.setState({
            filters: {
                city: '',
                clubName: '',
                league: '',
                position: ''
            }
        });
    }

    filterTests() {
        const {filters, availableTests} = this.state;
        let filteredTests = [...availableTests];

        Object.keys(filters).forEach(filter => {
            filteredTests = this.filterByFilterType(filter, filteredTests);
        });

        return filteredTests;
    }

    filterByFilterType(filter, tests) {
        return (fieldsFilteredByPhrase.includes(filter) && this.filterTestsByPhrase(filter, tests)) ||
            (fieldsFilteredByCompare.includes(filter) && this.filterTestsByComparing(filter, tests));
    }

    filterTestsByPhrase(filter, tests) {
        const {filters} = this.state;

        if (filters[filter] !== '') {
            return tests.filter(test => {
                return test[filter].toLowerCase().includes(filters[filter].toLowerCase());
            });
        }

        return tests;
    }

    filterTestsByComparing(filter, tests) {
        const {filters} = this.state;

        if (filters[filter] !== '') {
            return tests.filter(test => {
                return test[filter].toLowerCase() === filters[filter].toLowerCase();
            });
        }

        return tests;
    }

    addTest(newTest) {
        const {availableTests} = this.state;
        const id = availableTests[availableTests.length - 1].id + 1;

        newTest = Object.assign({}, {...newTest}, {id});

        this.setState({
            availableTests: [...availableTests, newTest]
        });

        firebase.addTest(newTest);

        Notifications.success('Test został pomyślnie dodany.', 'Dodawanie testu');

        this.toggleAddTestModal();
    }

    onEditTest(editedTest) {
        this.editTestOptimisticResponse(editedTest);

        const testId = this.state.selectedTest.id;

        firebase.updateTest(testId, editedTest).then(() => {
            this.refetchTests();

            Notifications.success('Test został pomyślnie zaaktualizowany.', 'Edycja testu');
        }).catch(() => {
            Notifications.error('Wystąpił błąd podczas aktualizacji testu.', 'Edycja testu');
        });
    }

    editTestOptimisticResponse(editedTest) {
        const testId = this.state.selectedTest.id;
        const availableTests = [...this.state.availableTests];
        availableTests[this.getTestIndexByTestId(testId)] = editedTest;
        this.setState({
            availableTests,
            editTestModalIsOpen: false,
            selectedTest: null
        });
    }

    onRemoveTest() {
        this.onRemoveTestOptimisticResponse();

        const {selectedTest} = this.state;
        firebase.removeTest(selectedTest.id).then(() => {
            this.refetchTests();

            Notifications.success('Test został pomyślnie usunięty.', 'Usuwanie testu');
        }).catch(() => {
            Notifications.error('Wystąpił błąd podczas usuwania testu.', 'Usuwanie testu');
        });
    }

    onRemoveTestOptimisticResponse() {
        const {selectedTest} = this.state;
        const availableTests = [...this.state.availableTests];
        availableTests.splice(this.getTestIndexByTestId(selectedTest.id), 1);
        this.setState({
            availableTests,
            confirmDeleteModalIsOpen: false,
            selectedTest: null
        });

    }

    toggleAddTestModal() {
        this.setState({
            addTestModalIsOpen: !this.state.addTestModalIsOpen
        });
    }

    openEditTestModal(testId) {
        this.setState({
            selectedTest: this.getTestById(testId),
            editTestModalIsOpen: true
        });
    }

    onRemoveTestClick(testId) {
        this.setState({
            selectedTest: this.getTestById(testId),
            confirmDeleteModalIsOpen: true
        });
    }

    onCloseConfirmRemoveTestModal() {
        this.setState({
            selectedTest: null,
            confirmDeleteModalIsOpen: false
        })
    }

    closeEditTestModal() {
        this.setState({
            selectedTest: null,
            editTestModalIsOpen: false
        });
    }

    selectTestAndOpenModal(test) {
        this.setState({
            testDetailModalIsOpen: true,
            selectedTest: test
        });
    }

    closeTestDetailsModal() {
        this.setState({
            testDetailModalIsOpen: false,
            selectedTest: null
        });
    }

    getTestById(testId, tests = this.state.availableTests) {
        return tests.filter(test => {
            return test.id === testId
        })[0];
    }

    getTestIndexByTestId(testId, tests = this.state.availableTests) {
        let index = -1;

        tests.forEach((test, i) => {
                if (test.id === testId) {
                    index = i;
                }
            }
        );

        return index;
    }

    get hasFilters() {
        const {filters} = this.state;
        return !!filters.city || !!filters.clubName || !!filters.league || !!filters.position;
    }

    refetchTests() {

        const availableTests = [];
        firebase.getTests().then(tests => {
            Object.keys(tests).forEach(key => {
                availableTests.push(tests[key]);
            });

            this.setState({
                availableTests,
                selectedTest: null
            });
        });
    }
}

export default App;
