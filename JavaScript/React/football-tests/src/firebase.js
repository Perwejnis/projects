import * as firebase from 'firebase';
let database;

export function init() {

    let config = {
        apiKey: "AIzaSyBocdSO5t-SWhWjdnydnlqU_lHE-gS0STc",
        authDomain: "footballtests-45a6b.firebaseapp.com",
        databaseURL: "https://footballtests-45a6b.firebaseio.com",
        projectId: "footballtests-45a6b",
        storageBucket: "footballtests-45a6b.appspot.com",
        messagingSenderId: "605917975465"
    };

    firebase.initializeApp(config);

    database = firebase.database();
}

export function getTests() {
    return database.ref('/tests').once('value')
        .then(snapshot => snapshot.val())
        .catch(e => console.log(e, 'e'));
}

export function addTest(newTest) {
    const key = database.ref('/tests').push().key;
    newTest.id = key;
    return database.ref('/tests/'+ key).set(newTest);
}

export function addMultipleTests(availableTests) {
    availableTests.forEach(test => {
        const key = database.ref('/tests').push().key;
        test.id = key;
        return database.ref('/tests/'+ key).set(test);
    });
}

export function removeTest(testId) {
    return database.ref(`/tests/` + testId).remove();
}

export function updateTest(testId, updatedTest) {
    return database.ref('/tests/'+ testId).update(updatedTest);
}
