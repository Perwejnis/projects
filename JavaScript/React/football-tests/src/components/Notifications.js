import {NotificationManager} from "react-notifications";

const Notifications = {
    success(message, title) {
        return NotificationManager.success(message, title);
    },
    error(message, title) {
        return NotificationManager.error(message, title);
    },
    warning(message, title) {
        return NotificationManager.warning(message, title);
    }
};

export default Notifications;