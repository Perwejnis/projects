import * as React from 'react';
import ReactLoading from 'react-loading';

export default function LoadingScreen() {
    const colorBlue = '#5cb3fd';
    return <div className="c-loading-screen">
        <ReactLoading type="spin" color={colorBlue}/>
    </div>;

};