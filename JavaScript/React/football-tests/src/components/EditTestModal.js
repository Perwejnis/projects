import * as React from 'react';
import PropTypes from 'prop-types';
import yup from 'yup';
import * as Modal from "./Modal";
import * as Form from './Form';
import PositionField from "./PositionField";
import LeagueField from "./LeagueField";
import CityAutocomplete from "./autocomplete/CityAutocomplete";
import ClubNameAutocomplete from "./autocomplete/ClubNameAutocomplete";

const schema = yup.object({
    city: yup.string().required('Miasto jest wymagane'),
    clubName: yup.string().required('Nazwa klubu jest wymagana'),
    position: yup.string().required('Pozycja jest wymagana'),
    league: yup.string().required('Klasa rozgrywkowa jest wymagana'),
    salary: yup.number().required('Wynagrodzenie jest wymagane'),
    vacancies: yup.number().required('Liczba miejsc jest wymagana'),
    contact: yup.string().required('Kontakt jest wymagany'),
});

export default class EditTestModal extends React.PureComponent {

    constructor(props) {
        super(props);

        this.onChange = this.onChange.bind(this);
        this.onEdit = this.onEdit.bind(this);

        this.state = {
            isValid: true
        }
    }

    render() {
        const {onClose, isOpen} = this.props;
        return <Modal.Modal
            isOpen={isOpen}
            onClose={onClose}
            isBig={true}
            title="Edytuj wybrany test"
        >
            <Modal.ModalContent>
                {this.renderForm()}
            </Modal.ModalContent>
            <Modal.ModalFooter>
                <Modal.ModalButtons
                    submitTitle="Edytuj test"
                    onCancel={onClose}
                    isDisabled={!this.state.isValid}
                    onSubmit={this.onEdit}
                    isSuccess={true}/>
            </Modal.ModalFooter>
        </Modal.Modal>;
    }

    renderForm() {
        return <Form.Form
            schema={schema}
            onSubmit={this.onEdit}
            onChange={this.onChange}
            defaultValue={this.props.selectedTest}
        >
            <Form.Input name="city" type={CityField} label="Miasto"/>
            <Form.Input name="clubName" type={ClubNameField} label="Nazwa klubu"/>
            <PositionField name="position" label="Pozycja"/>
            <LeagueField name="league" label="Klasa rozgrywkowa"/>
            <Form.Field type='number' name="salary" label="Wynagrodzenie" placeholder="Podaj wynagrodzenie"/>
            <Form.Field type='number' name="vacancies" label="Liczba wakatów" placeholder="Podaj liczbę miejsc..."/>
            <Form.Field name="contact" label="Kontakt" placeholder="Podaj numer kontaktowy..."/>
        </Form.Form>
    }

    onChange(editedTestModal) {
        schema.isValid(editedTestModal).then(isValid => {
            this.setState({
                isValid,
                editedTestModal
            });
        });
    }

    onEdit() {
        this.props.onEdit(this.state.editedTestModal);
    }

}

function CityField(props) {
    return <CityAutocomplete {...props}/>
}

function ClubNameField(props) {
    return <ClubNameAutocomplete {...props}/>;
}

EditTestModal.propTypes = {
    isOpen: PropTypes.bool.isRequired,
    onClose: PropTypes.func.isRequired,
    onEdit: PropTypes.func.isRequired,
    selectedTest: PropTypes.shape({
        city: PropTypes.string,
        clubName: PropTypes.string,
        position: PropTypes.string,
        league: PropTypes.string,
        salary: PropTypes.number,
        vacancies: PropTypes.number,
        contact: PropTypes.string
    })
};