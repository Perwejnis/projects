import * as React from 'react';
import FootballTests from "./components/FootballTests";
import * as firebase from './firebase'
import {NotificationContainer} from "react-notifications";

class App extends React.PureComponent {

    constructor(props) {
        super(props);

        firebase.init();
    }

    render() {
        return <div className="App">
            <FootballTests/>
            <NotificationContainer/>
        </div>;
    }
}

export default App;