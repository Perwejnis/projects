import * as React from 'react';
import PropTypes from 'prop-types';
import Formal from 'react-formal';
import classNames from 'classnames';

export const Form = Formal;
export const Input = Formal.Field;

export class Field extends React.PureComponent {

    constructor(props) {
        super(props);

        this.state = {
            fieldId: Math.random()
        };
    }

    render() {
        const {fieldId} = this.state;
        const {label, ...otherProps} = this.props;
        const fieldClasses = classNames('form-control c-form-field', this.getFieldClass());
        return <FieldWrapper>
            {!!label && <label className="col-form-label c-form-field__label" htmlFor={fieldId}>{label}</label>}
            <Form.Field className={fieldClasses} {...otherProps}/>
            <Form.Message for={otherProps.name}/>
        </FieldWrapper>;
    }

    getFieldClass() {
        const {type} = this.props;
        switch(type) {
            case 'select':
                return 'c-form-field--select';
            default:
                return 'c-form-field--input';
        }
    }
}

function FieldWrapper({children, ...otherProps}) {
    return <div className="c-form-field-wrapper" {...otherProps}>
        {children}
    </div>;
}

Field.propTypes = {
    label: PropTypes.node
};

Field.defaultProps = {
    type: 'text'
};
