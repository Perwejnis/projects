import * as React from 'react';
import PropTypes from 'prop-types';
import * as Modal from "./Modal";

export function TestDetailsModal({isOpen, onClose, selectedTest}) {
    return !!selectedTest && <Modal.Modal
        isOpen={isOpen}
        onClose={onClose}
        className="c-test-details-modal"
        title="Podgląd testu"
    >
            <Modal.ModalContent className="c-test-details-modal__content">
                <p>Miasto: {selectedTest.city}</p>
                <p>Nazwa klubu: {selectedTest.clubName}</p>
                <p>Pozycja: {selectedTest.position}</p>
                <p>Klasa rozgrywkowa: {selectedTest.league}</p>
                <p>Wynagrodzenie: {selectedTest.salary}</p>
                <p>Liczba miejsc: {selectedTest.vacancies}</p>
                <p>Kontakt: {selectedTest.contact}</p>
            </Modal.ModalContent>
        </Modal.Modal>;
}

TestDetailsModal.propTypes = {
    isOpen: PropTypes.bool.isRequired,
    onClose: PropTypes.func.isRequired,
    selectedTest: PropTypes.object
};
