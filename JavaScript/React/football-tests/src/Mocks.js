export const availableTests = [
    {
        id: 1,
        city: 'Gdynia',
        clubName: 'Arka Gdynia',
        league: 'Ekstraklasa',
        position: 'Napastnik',
        salary: 1500,
        vacancies: 5,
        contact: '123-456-789'
    },
    {
        id: 2,
        city: 'Gdańsk',
        clubName: 'Lechia Gdańsk',
        league: 'Ekstraklasa',
        position: 'Obrońca',
        salary: 1200,
        vacancies: 3,
        contact: 'test@lechia.pl'
    },
    {
        id: 3,
        city: 'Elbląg',
        clubName: 'Olimpia Elbląg',
        league: '2',
        position: 'Bramkarz',
        salary: 800,
        vacancies: 10,
        contact: '123-456-789'
    },
    {
        id: 4,
        city: 'Pasłęk',
        clubName: 'Polonia Pasłęk',
        position: 'Obrońca',
        league: '5',
        salary: 200,
        vacancies: 5,
        contact: 'test@paslek.pl'
    },
    {
        id: 5,
        city: 'Poznań',
        clubName: 'Lech Poznań',
        position: 'Pomocnik',
        league: 'Ekstraklasa',
        salary: 1700,
        vacancies: 5,
        contact: '123-456-789'
    },
    {
        id: 6,
        city: 'Poznań',
        clubName: 'Warta Poznań',
        position: 'Napastink',
        league: '2',
        salary: 2000,
        vacancies: 5,
        contact: 'test@lech.pl'
    }
];