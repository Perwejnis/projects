import * as React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import ReactAutocomplete from 'react-autocomplete'
import {Form} from '../Form';

export default class Autocomplete extends React.PureComponent {

    constructor(props) {
        super(props);

        this.state = {
            fieldId: Math.random()
        };

        this.onChange = this.onChange.bind(this);
    }

    render() {
        const {fieldId} = this.state;
        const {value, items, onChange, placeholder, label, name, className, ...otherProps} = this.props;
        const wrapperClasses = classNames("c-form-field-wrapper c-autocomplete", className);
        const wrapperProps = {className: 'c-form-field-wrapper'};
        const inputProps = {
            className: classNames('form-control', 'c-form-field', {'invalid-field': !!otherProps.meta.errors && !!otherProps.meta.errors[name]}),
            name: name,
            id: fieldId,
            placeholder
        };

        return <div className={wrapperClasses}>
            {!!label && <label className="col-form-label c-form-field__label" htmlFor={fieldId}>{label}</label>}
            <ReactAutocomplete
                items={items}
                shouldItemRender={this.shouldRenderItem}
                getItemValue={item => item.label}
                renderItem={this.renderItem}
                wrapperProps={wrapperProps}
                inputProps={inputProps}
                value={value}
                onChange={this.onChange}
                onSelect={onChange}
            />
            <Form.Message for={name}/>
        </div>;
    }

    onChange(event) {
        this.props.onChange(event.target.value);
    }

    shouldRenderItem(item, value) {
        return item.label.toLowerCase().indexOf(value.toLowerCase()) > -1;
    }

    renderItem(item, highlighted) {
        const classes = classNames('c-autocomplete__item', {'c-autocomplete__item--highlighted': highlighted});
        return <div
            className={classes}
            key={item.id}
        >
            {item.label}
        </div>;
    }
}

const dataPropTypeShape = PropTypes.shape({
    id: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired
});

Autocomplete.propTypes = {
    items: PropTypes.arrayOf(dataPropTypeShape).isRequired,
    value: PropTypes.string,
    label: PropTypes.string,
    placeholder: PropTypes.string,
    onChange: PropTypes.func.isRequired
};

Autocomplete.defaultProps = {
    value: '',
    placeholder: "Wyszukaj..."
};